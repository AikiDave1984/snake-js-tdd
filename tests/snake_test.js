
var expect = chai.expect;
var assert = chai.assert;

if(typeof constants === 'undefined'){
	throw new Error('constants not defined');
}

if(typeof Point === 'undefined'){
	throw new Error('Point function not defined');
}
describe("Snake Function", function(){
	
	var snake = new Snake();
	
	it('should have a direction property and default to constants.DEFAULT_DIRECTION',function(){
		expect(snake).to.have.property('direction');
		expect(snake.direction).to.equal(constants.DEFAULT_DIRECTION);
	});
	
	it('should have a points property and default to empty array',function(){
		expect(snake).to.have.property('points');
		assert.deepEqual(snake.points,[]);
	});
	
	it('should have a growthLeft property and default to 0',function(){
		expect(snake).to.have.property('growthLeft');
		expect(snake.growthLeft).to.equal(0);
	});
	
	it('should have a isAlive property and default to true',function(){
		expect(snake).to.have.property('isAlive');
		expect(snake.isAlive).to.be.true;
	});
	
	it('should have a collidesWith method',function(){
		expect(snake).to.have.property('collidesWith');
		assert.isFunction(snake.collidesWith ,' is a function');
	});
	
	describe("snake.collidesWith method", function(){
		it('should have two parameters',function(){
			expect(snake.collidesWith.length).to.be.equal(2);
		});
		
		var snake = new Snake();
		
		snake.points.push(new Point(0,1));
		snake.points.push(new Point(0,2));
		
		var collisionPoint = new Point(0,2);
		var missedPoint = new Point(1,1);
		
		it('should not mark a collision with the last point when the simulateMovement is true and growth left = 0',function(){
			expect(snake.collidesWith(collisionPoint,true)).to.be.false;
		});
		
		it('should mark a collision with the last point when the simulateMovement is false and growth left = 0',function(){
			expect(snake.collidesWith(collisionPoint,false)).to.be.true;
		});
		
		it('should mark a collision with the last point when the simulateMovement is true and growth left > 0',function(){
			snake.growthLeft = 1;
			expect(snake.collidesWith(collisionPoint,true)).to.be.true;
		});
		
		it('should mark a collision with the last point when the simulateMovement is false and growth left > 0',function(){
			snake.growthLeft = 2;
			expect(snake.collidesWith(collisionPoint,false)).to.be.true;
		});
		
		it('should have return false if point does not equal each point in the snake',function(){
			snake.growthLeft = 0;
			expect(snake.collidesWith(missedPoint)).to.be.false;
		});
		
		it('should have return true if point matches a single point in the snake',function(){
			expect(snake.collidesWith(collisionPoint,false)).to.be.true;
		});
	});
});