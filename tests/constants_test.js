
var expect = chai.expect;
var assert = chai.assert;

describe("Constants Object", function(){

	it('should have a DIRECTION_UP property',function(){
		expect(constants).to.have.property('DIRECTION_UP');
	});
	
	it('should have a DIRECTION_RIGHT property',function(){
		expect(constants).to.have.property('DIRECTION_RIGHT');
	});
	
	it('should have a DIRECTION_DOWN property',function(){
		expect(constants).to.have.property('DIRECTION_DOWN');
	});
	
	it('should have a DIRECTION_LEFT property',function(){
		expect(constants).to.have.property('DIRECTION_LEFT');
	});
	
	it('should have a DEFAULT_DIRECTION property',function(){
		expect(constants).to.have.property('DEFAULT_DIRECTION');
	});
	
	it('should have a STATE_READY property',function(){
		expect(constants).to.have.property('STATE_READY');
	});
	
	it('should have a STATE_PAUSED property',function(){
		expect(constants).to.have.property('STATE_PAUSED');
	});
	
	it('should have a STATE_PLAYING property',function(){
		expect(constants).to.have.property('STATE_PLAYING');
	});
	
	it('should have a STATE_GAME_OVER property',function(){
		expect(constants).to.have.property('STATE_GAME_OVER');
	});
	
	it('should have a INITIAL_SNAKE_GROWTH_LEFT property',function(){
		expect(constants).to.have.property('INITIAL_SNAKE_GROWTH_LEFT');
	});
	
	it('should have a SCOREBOARD_HEIGHT property',function(){
		expect(constants).to.have.property('SCOREBOARD_HEIGHT');
	});
	
	it('should have a CANDY_REGULAR property',function(){
		expect(constants).to.have.property('CANDY_REGULAR');
	});
	
	it('should have a CANDY_MASSIVE property',function(){
		expect(constants).to.have.property('CANDY_MASSIVE');
	});
	
	it('should have a CANDY_SHRINKING property',function(){
		expect(constants).to.have.property('CANDY_SHRINKING');
	});
});
