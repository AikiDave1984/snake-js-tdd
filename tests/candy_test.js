var expect = chai.expect;
var assert = chai.assert;

if(typeof constants === 'undefined'){
	throw new Error('constants not defined');
}

if(typeof defaultConfig === 'undefined'){
	throw new Error('defaultConfig not defined');
}

describe("Candy", function(){
	
	var candy = new Candy();

	it('should have 2 parameters', function(){
		expect(Candy.length).to.equal(2);
	});
	
	it('should have a point property',function(){
		expect(candy).to.have.property('point');
	});
	
	it('should have a type property',function(){
		expect(candy).to.have.property('type');
	});
	
	it('should have a score property with initial value 0',function(){
		expect(candy).to.have.property('score');
		expect(candy.score).to.equal(0);
	});
	
	it('should have a color property with default value as #FFF',function(){
		expect(candy).to.have.property('color');
		expect(candy.color).to.equal('#FFF');
	});
	
	it('should have a calories property with default value = 0',function(){
		expect(candy).to.have.property('calories');
		expect(candy.calories).to.equal(0);
	});
	
	it('should have a radius property with default value = 0',function(){
		expect(candy).to.have.property('radius');
		expect(candy.radius).to.equal(0);
	});
	
	it('should have a decrement property with default value = 0',function(){
		expect(candy).to.have.property('decrement');
		expect(candy.decrement).to.equal(0);
	});
	
	it('should have a minRadius property with default value = 0',function(){
		expect(candy).to.have.property('minRadius');
		expect(candy.minRadius).to.equal(0);
	});
	
	it('should have an age method',function(){
		expect(candy).to.have.property('age');
		assert.isFunction(candy.age ,' is a function');
	});
	
	describe('candy.age method',function(){
		it('should return true if not a shrinking candy',function(){
			var massiveCandy = new Candy({},constants.CANDY_MASSIVE);
			var regularCandy = new Candy({},constants.CANDY_REGULAR);
			expect(massiveCandy.age()).to.be.true;
			expect(regularCandy.age()).to.be.true;
		});
		
		it('should reduce the radius of a shrinking candy by decrement value and return true if radius - decrement >= minRadius',function(){
			var shrinkingCandy = new Candy({},constants.CANDY_SHRINKING);
			var hasShrunk = shrinkingCandy.age();
			expect(shrinkingCandy.radius).to.be.equal(0.442);
			expect(hasShrunk).to.be.true;
		});
		
		it('should reduce the radius of a shrinking candy by decrement value and return false if radius - decrement < minRadius',function(){
			var shrinkingCandy = new Candy({},constants.CANDY_SHRINKING);
			shrinkingCandy.radius = 0.01;
			var hasShrunk = shrinkingCandy.age();
			expect(shrinkingCandy.radius).to.be.equal(0.002);
			expect(hasShrunk).to.be.false;
		});
	})
	
	
	describe('Regular candy instance',function(){
		it('should create an instance with score = 5, calories =3 radius =0.3 and color = red (stub)',function(){
			var regularCandy = new Candy({},constants.CANDY_REGULAR);
			expect(regularCandy.score).to.equal(5);
			expect(regularCandy.calories).to.equal(3);
			expect(regularCandy.radius).to.equal(0.3);
			expect(regularCandy.color).to.equal(defaultConfig.candyColor);
		})
	});
	
	describe('Massive candy instance',function(){
		it('should create an instance with score = 15, calories =5 radius =0.45 and color = blue(stub)',function(){
			var massiveCandy = new Candy({},constants.CANDY_MASSIVE);
			expect(massiveCandy.score).to.equal(15);
			expect(massiveCandy.calories).to.equal(5);
			expect(massiveCandy.radius).to.equal(0.45);
			expect(massiveCandy.color).to.equal(defaultConfig.candyColor);
		})
	});
	
	describe('Regular candy instance',function(){
		it('should create an instance with score = 50, calories =0 radius =0.45,decrement = 0.008, minRadius =0.05 and color = green (stub)',function(){
			var shrinkingCandy = new Candy({},constants.CANDY_SHRINKING);
			expect(shrinkingCandy.score).to.equal(50);
			expect(shrinkingCandy.calories).to.equal(0);
			expect(shrinkingCandy.radius).to.equal(0.45);
			expect(shrinkingCandy.decrement).to.equal(0.008);
			expect(shrinkingCandy.minRadius).to.equal(0.05);
			expect(shrinkingCandy.color).to.equal(defaultConfig.shrinkingCandyColor);
		})
	});
});