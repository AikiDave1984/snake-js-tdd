var expect = chai.expect;
var assert = chai.assert;

describe("Point Function", function(){
	
	var point = new Point(0,0);
	
	it('should have a left property',function(){
		expect(point).to.have.property('left');
	});
	
	it('should have a top property',function(){
		expect(point).to.have.property('top');
	});
	
	it('should have two parameters',function(){
		expect(Point.length).to.equal(2);
	});
	
	it('should have a collidesWith method',function(){
		expect(point).to.have.property('collidesWith');
		assert.isFunction(point.collidesWith ,' is a function');
	});
	
	describe("point.collidesWith method", function(){
		it('should have a single parameter',function(){
			expect(point.collidesWith.length).to.be.equal(1);
		});
		
		it('should have return false if argument point obj has different top,left properties',function(){
			expect(point.collidesWith(new Point(1,1))).to.be.false;
		});
		
		it('should have return true if argument point obj has same top,left properties',function(){
			expect(point.collidesWith(new Point(0,0))).to.be.true;
		});
	});
});