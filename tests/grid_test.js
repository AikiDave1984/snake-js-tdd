
var expect = chai.expect;
var assert = chai.assert;

describe("Grid Function", function(){
	
	var grid = new Grid();
	
	it('should have a width property',function(){
		expect(grid).to.have.property('width');
	});
	
	it('should have a height property',function(){
		expect(grid).to.have.property('height');
	});
	
});