
var expect = chai.expect;
var assert = chai.assert;

describe("Config Function", function(){

	var configObj = {};

	var config = new Config(configObj);

	it('should have a single parameter',function(){
		expect(Config.length).to.equal(1);
	});
	
	it('should override the default config if property set in config obj',function(){
		
		var configObj = {backgroundColor:'black'};
		var config = new Config(configObj);

		expect(config.backgroundColor).to.equal('black');
	});
	
	it('should return the default config if configObj property is not set',function(){
		
		var config = new Config();

		expect(config.backgroundColor).to.equal('white');
	});
	
});