var ContextTester = function(canvasElement){
	this._ctx = canvasElement.getContext('2d');
	this._calls = [];
	
	this._initMethods = function(){
		
		var methods = {
			arc:function(x, y, radius, startAngle, endAngle, isAnticlockwise){
				this._ctx.arc(x, y, radius, startAngle, endAngle, isAnticlockwise);
			},
			beginPath:function(){
				this._ctx.beginPath();
			},
			fill:function(){
				this._ctx.fill();
			},
			lineTo:function(x,y){
				this._ctx.lineTo(x,y);
			},
			moveTo:function(x,y){
				this._ctx.moveTo(x,y);
			},
			stroke:function(){
				this._ctx.stroke();
			}
			
		}
		
		var scope = this;

		var addMethod = function(name,method){
			scope[methodName] = function(){
				scope.record(name,arguments);
				method.apply(scope,arguments);
			}
		}
			
		for(var methodName in methods){
			var method = methods[methodName];		
			addMethod(methodName, method);
		}
		
	};
	
	this.assign = function(k,v){
		this._ctx[k] = v;
	}
	
	this.record = function(methodName,args){
		this._calls.push({name:methodName,args:args});
	}
	
	this.getCalls = function(){
		return this._calls;
	}
	
	this._initMethods();
	
	
	
}
/*
// Usage
var ctx = new Context(document.getElementById('myCanvas'));

ctx.moveTo(34, 54);
ctx.lineTo(63, 12);

ctx.assign('strokeStyle', "#FF00FF");
ctx.stroke();

var calls = ctx.getCalls();

console.log(calls);
*/