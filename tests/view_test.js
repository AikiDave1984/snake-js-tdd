

var expect = chai.expect;
var assert = chai.assert;

describe("View Function", function(){
	
	var view = new View();
	
	it('should have 2 parameters',function(){
		expect(View.length).to.equal(2);
	});
	
	it('should have an initPlayField method',function(){
		expect(view).to.have.property('initPlayField');
		assert.isFunction(view.initPlayField ,' is a function');
	});

	describe('initPlayField method', function(){
		it('should create a canvas element and attach to the parentElement argument',function(){
			var parentElement = document.createElement('div');
			var view = new View(parentElement);
			view.initPlayField();
			
			expect(parentElement.querySelector('#snake-js').id).to.equal('snake-js');
		});
	});
	
	it('should have a drawSnake method',function(){
				assert.isFunction(view.drawSnake,'drawSnake function present');
	});
	
	describe("private methods/properties made public", function(){
		it('should have a playField private property',function(){
				assert.isUndefined(view.api._playField,'playfield property present');
		});

		it('should have a ctx private property',function(){
				assert.isUndefined(view.api._ctx,'ctx property present');
		});
		
		it('should have a snakeThickness private property',function(){
				assert.isUndefined(view.api._snakeThickness,'snakeThickness property present');
		});
		
		it('should have a length private method',function(){
				assert.isFunction(view.api._length,'length function present');
		});
		
		it('should have a getPointPosition private method',function(){
				assert.isFunction(view.api._getPointPivotPosition,'getPointPivotPosition function present');
		});
		
		
		
		describe('length method', function(){
			
			it('should have a single parameter',function(){
				expect(view.api._length.length).to.equal(1);
			});
			
			it('should return a decimal value',function(){
				var value = 1;
				var config = new Config();
				expect(view.api._length(value)).to.equal(value * config.pointSize);
			})
		})
		
		describe('getPointPivotPosition method', function(){
			it('should have a single argument', function(){
				expect(view.api._getPointPivotPosition.length).to.equal(1);
			});
			
			it('should return a position object' , function(){
				var point = new Point(1,1);
				var actualObject = view.api._getPointPivotPosition(point);
				var expectedObject = {left: 24, top:24};
				
				expect(actualObject).to.deep.equal(expectedObject);
				
			});
		})
		
		describe('drawSnake method', function(){
			it('should have two arguments', function(){
				expect(view.drawSnake.length).to.equal(2);
			});
			
			describe('drawSnake method - render snake with one point', function(){
				it('should render the canvas with these methods', function(){
					
					var snake = new Snake();
					
					var point = new Point(1,1);
					
					snake.points.push(point);

					var parentElement = document.createElement('div');
					var view = new View(parentElement,'#FFF');

					view.initPlayField();
					view.drawSnake(snake,'blue');
					
					var callsMade = view.api._ctx.getCalls();
					
					expect(callsMade[0].name).to.equal("beginPath");
					expect(callsMade[0].args.length).to.equal(0);
					
					expect(callsMade[1].name).to.equal("arc");
					expect(callsMade[1].args[0]).to.equal(24);
					expect(callsMade[1].args[1]).to.equal(24);
					expect(callsMade[1].args[2]).to.equal(7.2);
					expect(callsMade[1].args[3]).to.equal(0);
					expect(callsMade[1].args[4]).to.equal(2*Math.PI);
					expect(callsMade[1].args[5]).to.be.false;
					
					expect(callsMade[2].name).to.equal("fill");
					expect(callsMade[2].args.length).to.equal(0);
					
				});
			})
			
			describe('drawSnake method - render snake with multiple points', function(){
				it('should render the canvas with these methods', function(){
					
					var snake = new Snake();
					
					var point = new Point(1,1);
					var point1 = new Point(1,2);
					
					snake.points.push(point);
					snake.points.push(point1);

					var parentElement = document.createElement('div');
					var view = new View(parentElement,'#FFF');

					view.initPlayField();
					view.drawSnake(snake,'blue');
					
					var callsMade = view.api._ctx.getCalls();
					
					expect(callsMade[0].name).to.equal("beginPath");
					expect(callsMade[0].args.length).to.equal(0);
					
					expect(callsMade[1].name).to.equal("moveTo");
					expect(callsMade[1].args[0]).to.equal(24);
					expect(callsMade[1].args[1]).to.equal(24);
					
					expect(callsMade[2].name).to.equal("lineTo");
					expect(callsMade[2].args[0]).to.equal(24);
					expect(callsMade[2].args[1]).to.equal(40);
					
					expect(callsMade[3].name).to.equal("stroke");
					expect(callsMade[3].args.length).to.equal(0);
				});
			})
			
		})
	
		
	});
	
});

