
var expect = chai.expect;
var assert = chai.assert;

if(typeof constants === 'undefined'){
	throw new Error('constants not defined');
}

describe("InputInterface Function", function(){
	
	var inputInterface = new InputInterface();
	
	it('should have 3 parameters',function(){
		expect(InputInterface.length).to.equal(3);
	});
	
	it('should have a lastDirection method',function(){
		expect(inputInterface).to.have.property('lastDirection');
		assert.isFunction(inputInterface.lastDirection ,' is a function');
	});
	
	it('should have a startListening method',function(){
		expect(inputInterface).to.have.property('startListening');
		assert.isFunction(inputInterface.startListening ,' is a function');
	});
	
	it('should have a stopListening method',function(){
		expect(inputInterface).to.have.property('stopListening');
		assert.isFunction(inputInterface.stopListening ,' is a function');
	});
	
	describe("private methods/properties made public", function(){
		it('should have an arrowKeys array = [37,38,39,40]',function(){
				expect(inputInterface.api._arrowKeys).to.deep.equal([37,38,39,40]);
		});

		it('should have a lastDirection variable set to null on initialisation',function(){
				expect(inputInterface.api._lastDirection).to.equal(null);
		});
		
		it('should have a listening variable set to false on initialisation',function(){
				expect(inputInterface.api._listening).to.be.false;
		});
		
		it('should have a handleKeyDown method',function(){
				assert.isFunction(inputInterface.api._handleKeyDown ,' is a function');
		});
	});
	
	describe("inputInterface.lastDirection Method", function(){
		it('should return lastDirection value',function(){
		 expect(inputInterface.lastDirection()).to.be.null;
		})
	});
	
	describe("inputInterface.startListening Method", function(){
		
		var val = 0;
		
		var pauseFn = function(){val = 1;}
		var resumeFn = function(){assert.isOk('everything','everything is ok');}
		var autoPlayFn = function(){assert.isOk('everything','everything is ok');;}
		
		var inputInterface = new InputInterface(pauseFn,resumeFn,autoPlayFn);

		it('should set key down event listeners and handle left arrow key input',function(){
			
			inputInterface.startListening();
			
			fireKey(window,37);
			expect(inputInterface.lastDirection()).to.equal(constants.DIRECTION_LEFT);
		})
		
		it('should set key down event listeners and handle up arrow key input',function(){
			
			inputInterface.startListening();
			
			fireKey(window,38);
			expect(inputInterface.lastDirection()).to.equal(constants.DIRECTION_UP);
		})
		
		it('should set key down event listeners and handle right arrow key input',function(){
			
			inputInterface.startListening();
			
			fireKey(window,39);
			expect(inputInterface.lastDirection()).to.equal(constants.DIRECTION_RIGHT);
		})
		
		it('should set key down event listeners and handle down arrow key input',function(){
			
			inputInterface.startListening();
			
			fireKey(window,40);
			expect(inputInterface.lastDirection()).to.equal(constants.DIRECTION_DOWN);
		})

		//write how to test blur and focus events
	});
	
	describe("inputInterface.stopListening Method", function(){
		it('should stop key down event listeners',function(){
			
			var inputInterface = new InputInterface();
			inputInterface.startListening();
			inputInterface.stopListening();
			fireKey(window,37);
			expect(inputInterface.lastDirection()).to.be.null;
		})
	});
});



function fireBlurEvent(el){
	el.blur();
}

function fireFocusEvent(el){
	el.focus();
}

function fireKey(el,key)
{
    //Set key to corresponding code. This one is set to the left arrow key.
    if(document.createEventObject)
    {
        var eventObj = document.createEventObject();
        eventObj.keyCode = key;
        el.fireEvent("onkeydown", eventObj);   
    }else if(document.createEvent)
    {
        var eventObj = document.createEvent("Events");
        eventObj.initEvent("keydown", true, true);
        eventObj.which = key;
        el.dispatchEvent(eventObj);
    }
} 