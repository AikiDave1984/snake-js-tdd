
var expect = chai.expect;
var assert = chai.assert;

describe("Default Config Object", function(){

	it('should have a autoInit property',function(){
		expect(defaultConfig).to.have.property('autoInit');
	});
	
	it('should have a gridWidth property',function(){
		expect(defaultConfig).to.have.property('gridWidth');
	});
	
	it('should have a gridHeight property',function(){
		expect(defaultConfig).to.have.property('gridHeight');
	});
	
	it('should have a frameInterval property',function(){
		expect(defaultConfig).to.have.property('frameInterval');
	});
	
	it('should have a pointSize property',function(){
		expect(defaultConfig).to.have.property('pointSize');
	});
	
	it('should have a backgroundColor property',function(){
		expect(defaultConfig).to.have.property('backgroundColor');
	});
	
	it('should have a snakeColor property',function(){
		expect(defaultConfig).to.have.property('snakeColor');
	});
	
	it('should have a snakeEyeColor property',function(){
		expect(defaultConfig).to.have.property('snakeEyeColor');
	});
	
	it('should have a candyColor property',function(){
		expect(defaultConfig).to.have.property('candyColor');
	});
	
	it('should have a shrinkingCandyColor property',function(){
		expect(defaultConfig).to.have.property('shrinkingCandyColor');
	});
	
	it('should have a scoreBoardColor property',function(){
		expect(defaultConfig).to.have.property('scoreBoardColor');
	});
	
	it('should have a scoreTextColor property',function(){
		expect(defaultConfig).to.have.property('scoreTextColor');
	});
	
	it('should have a collisionTolerance property',function(){
		expect(defaultConfig).to.have.property('collisionTolerance');
	});
	
});
