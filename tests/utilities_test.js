
var expect = chai.expect;
var assert = chai.assert;

describe("Utilities", function(){
	
	var utilities = new Utilities();
	
	it('should have a sign property function', function(){
			expect(utilities).to.have.property('sign');
			assert.isFunction(utilities.sign ,' is a function');
	});
	
	describe('utilities.sign',function(){
		it('should return 1 if argument > 0', function(){
			expect(utilities.sign(12)).to.equal(1);
		});
		
		it('should return -1 if argument < 0', function(){
			expect(utilities.sign(-12)).to.equal(-1);
		});
		
		it('should return 0 if argument = 0', function(){
			expect(utilities.sign(0)).to.equal(0);
		});
		
		it('should return return undefined if argument is NaN or undefined', function(){
			var isNotANumber = NaN;
			var isUndefined = undefined
			expect(utilities.sign(isNotANumber )).to.equal(undefined);
			expect(utilities.sign(isUndefined )).to.equal(undefined);
		});
	});
	
	it('should have an oppositeDirections property function', function(){
			expect(utilities).to.have.property('oppositeDirections');
			assert.isFunction(utilities.oppositeDirections ,' is a function');
	});
	
	describe('utilites.oppositeDirection', function(){

		var left = -1, right =1, top=2, bottom=-2;
		it('should have 2 parameters', function(){
			expect(utilities.oppositeDirections.length).to.equal(2);
		});
		
		it('should return true with left = -1 and right = 1', function(){
			expect(utilities.oppositeDirections(left,right)).to.be.true;
		});
		
		it('should return false with two left = -1 parameters', function(){
			expect(utilities.oppositeDirections(left,left)).to.be.false;
		});
		
		it('should return false with left = -1 and top = 2 parameters', function(){
			expect(utilities.oppositeDirections(left,top)).to.be.false;
		});
	});
	
	it('should have a mergeObjects property function', function(){
			expect(utilities).to.have.property('mergeObjects');
			assert.isFunction(utilities.mergeObjects ,' is a function');
	});
	
	describe('utilites.mergeObjects', function(){

		it('should have 2 parameters', function(){
			expect(utilities.mergeObjects.length).to.equal(2);
		});
		
		var empty1 = {},empty2 = {},stub={a:1,b:3},stub1 = {a:-22};
		
		it('should merge two empty objects and return an empty object', function(){
			var mergedObject = utilities.mergeObjects(empty1,empty2)
			assert.deepEqual(mergedObject,{});
		});
		
		it('should merge a object with an empty object and return the master object', function(){
			var mergedObject = utilities.mergeObjects(empty1,stub);
			assert.deepEqual(mergedObject,{a:1,b:3});
		});
		
		it('should merge two objects and return this new object', function(){
			var mergedObject = utilities.mergeObjects(stub,stub1);
			assert.deepEqual(mergedObject,{a:-22,b:3});
		});
		
	});
	
	it('should have a randomInteger property function', function(){
			expect(utilities).to.have.property('randomInteger');
			assert.isFunction(utilities.randomInteger ,' is a function');
	});
	
	describe('utilites.randomInteger', function(){

		it('should have 2 parameters', function(){
			expect(utilities.randomInteger.length).to.equal(2);
		});
		
		
		it('handle positive integers', function(){
			var randomInteger = utilities.randomInteger(1,4);
			expect(randomInteger).to.be.within(1,4);
		});

		it('handle negative integers', function(){
			var randomInteger = utilities.randomInteger(-10,-4);
			expect(randomInteger).to.be.within(-10,-4);
		});
		
		it('handle positive and negative integers', function(){
			var randomInteger = utilities.randomInteger(-10,4);
			expect(randomInteger).to.be.within(-10,4);
		});
		
	});
});
	



