
[0m[0m
[0m  Candy[0m
  [32m  ✓[0m[90m should have 2 parameters[0m
  [32m  ✓[0m[90m should have a point property[0m
  [32m  ✓[0m[90m should have a type property[0m
  [32m  ✓[0m[90m should have a score property with initial value 0[0m
  [32m  ✓[0m[90m should have a color property with default value as #FFF[0m
  [32m  ✓[0m[90m should have a calories property with default value = 0[0m
  [32m  ✓[0m[90m should have a radius property with default value = 0[0m
  [32m  ✓[0m[90m should have a decrement property with default value = 0[0m
  [32m  ✓[0m[90m should have a minRadius property with default value = 0[0m
  [32m  ✓[0m[90m should have an age method[0m
[0m    candy.age method[0m
    [32m  ✓[0m[90m should return true if not a shrinking candy[0m
    [32m  ✓[0m[90m should reduce the radius of a shrinking candy by decrement value and return true if radius - decrement >= minRadius[0m
    [32m  ✓[0m[90m should reduce the radius of a shrinking candy by decrement value and return false if radius - decrement < minRadius[0m
[0m    Regular candy instance[0m
    [32m  ✓[0m[90m should create an instance with score = 5, calories =3 radius =0.3 and color = red (stub)[0m
[0m    Massive candy instance[0m
    [32m  ✓[0m[90m should create an instance with score = 15, calories =5 radius =0.45 and color = blue(stub)[0m
[0m    Regular candy instance[0m
    [32m  ✓[0m[90m should create an instance with score = 50, calories =0 radius =0.45,decrement = 0.008, minRadius =0.05 and color = green (stub)[0m

[0m  Constants Object[0m
  [32m  ✓[0m[90m should have a DIRECTION_UP property[0m
  [32m  ✓[0m[90m should have a DIRECTION_RIGHT property[0m
  [32m  ✓[0m[90m should have a DIRECTION_DOWN property[0m
  [32m  ✓[0m[90m should have a DIRECTION_LEFT property[0m
  [32m  ✓[0m[90m should have a DEFAULT_DIRECTION property[0m
  [32m  ✓[0m[90m should have a STATE_READY property[0m
  [32m  ✓[0m[90m should have a STATE_PAUSED property[0m
  [32m  ✓[0m[90m should have a STATE_PLAYING property[0m
  [32m  ✓[0m[90m should have a STATE_GAME_OVER property[0m
  [32m  ✓[0m[90m should have a INITIAL_SNAKE_GROWTH_LEFT property[0m
  [32m  ✓[0m[90m should have a SCOREBOARD_HEIGHT property[0m
  [32m  ✓[0m[90m should have a CANDY_REGULAR property[0m
  [32m  ✓[0m[90m should have a CANDY_MASSIVE property[0m
  [32m  ✓[0m[90m should have a CANDY_SHRINKING property[0m

[0m  Default Config Object[0m
  [32m  ✓[0m[90m should have a autoInit property[0m
  [32m  ✓[0m[90m should have a gridWidth property[0m
  [32m  ✓[0m[90m should have a gridHeight property[0m
  [32m  ✓[0m[90m should have a frameInterval property[0m
  [32m  ✓[0m[90m should have a pointSize property[0m
  [32m  ✓[0m[90m should have a backgroundColor property[0m
  [32m  ✓[0m[90m should have a snakeColor property[0m
  [32m  ✓[0m[90m should have a snakeEyeColor property[0m
  [32m  ✓[0m[90m should have a candyColor property[0m
  [32m  ✓[0m[90m should have a shrinkingCandyColor property[0m
  [32m  ✓[0m[90m should have a scoreBoardColor property[0m
  [32m  ✓[0m[90m should have a scoreTextColor property[0m
  [32m  ✓[0m[90m should have a collisionTolerance property[0m

[0m  Grid Function[0m
  [32m  ✓[0m[90m should have a width property[0m
  [32m  ✓[0m[90m should have a height property[0m

[0m  InputInterface Function[0m
  [32m  ✓[0m[90m should have 3 parameters[0m
  [32m  ✓[0m[90m should have a lastDirection method[0m
  [32m  ✓[0m[90m should have a startListening method[0m
  [32m  ✓[0m[90m should have a stopListening method[0m
[0m    inputInterface.lastDirection Method[0m
    [32m  ✓[0m[90m should return lastDirection value[0m
[0m    inputInterface.startListening Method[0m
    [31m  1) should set key down event listeners and handle left arrow key input[0m
    [31m  2) should set key down event listeners and handle up arrow key input[0m
    [31m  3) should set key down event listeners and handle right arrow key input[0m
    [31m  4) should set key down event listeners and handle down arrow key input[0m
[0m    inputInterface.stopListening Method[0m
    [31m  5) should stop key down event listeners[0m

[0m  Point Function[0m
  [32m  ✓[0m[90m should have a left property[0m
  [32m  ✓[0m[90m should have a top property[0m
  [32m  ✓[0m[90m should have two parameters[0m
  [32m  ✓[0m[90m should have a collidesWith method[0m
[0m    point.collidesWith method[0m
    [32m  ✓[0m[90m should have a single parameter[0m
    [32m  ✓[0m[90m should have return false if argument point obj has different top,left properties[0m
    [32m  ✓[0m[90m should have return true if argument point obj has same top,left properties[0m

[0m  Snake Function[0m
  [32m  ✓[0m[90m should have a direction property and default to constants.DEFAULT_DIRECTION[0m
  [32m  ✓[0m[90m should have a points property and default to empty array[0m
  [32m  ✓[0m[90m should have a growthLeft property and default to 0[0m
  [32m  ✓[0m[90m should have a isAlive property and default to true[0m
  [32m  ✓[0m[90m should have a collidesWith method[0m
[0m    snake.collidesWith method[0m
    [32m  ✓[0m[90m should have two parameters[0m
    [32m  ✓[0m[90m should not mark a collision with the last point when the simulateMovement is true and growth left = 0[0m
    [32m  ✓[0m[90m should mark a collision with the last point when the simulateMovement is false and growth left = 0[0m
    [32m  ✓[0m[90m should mark a collision with the last point when the simulateMovement is true and growth left > 0[0m
    [32m  ✓[0m[90m should mark a collision with the last point when the simulateMovement is false and growth left > 0[0m
    [32m  ✓[0m[90m should have return false if point does not equal each point in the snake[0m
    [32m  ✓[0m[90m should have return true if point matches a single point in the snake[0m

[0m  Utilities[0m
  [32m  ✓[0m[90m should have a sign property function[0m
  [32m  ✓[0m[90m should have an oppositeDirections property function[0m
  [32m  ✓[0m[90m should have a mergeObjects property function[0m
  [32m  ✓[0m[90m should have a randomInteger property function[0m
[0m    utilities.sign[0m
    [32m  ✓[0m[90m should return 1 if argument > 0[0m
    [32m  ✓[0m[90m should return -1 if argument < 0[0m
    [32m  ✓[0m[90m should return 0 if argument = 0[0m
    [32m  ✓[0m[90m should return return undefined if argument is NaN or undefined[0m
[0m    utilites.oppositeDirection[0m
    [32m  ✓[0m[90m should have 2 parameters[0m
    [32m  ✓[0m[90m should return true with left = -1 and right = 1[0m
    [32m  ✓[0m[90m should return false with two left = -1 parameters[0m
    [32m  ✓[0m[90m should return false with left = -1 and top = 2 parameters[0m
[0m    utilites.mergeObjects[0m
    [32m  ✓[0m[90m should have 2 parameters[0m
    [32m  ✓[0m[90m should merge two empty objects and return an empty object[0m
    [32m  ✓[0m[90m should merge a object with an empty object and return the master object[0m
    [32m  ✓[0m[90m should merge two objects and return this new object[0m
[0m    utilites.randomInteger[0m
    [32m  ✓[0m[90m should have 2 parameters[0m
    [32m  ✓[0m[90m handle positive integers[0m
    [32m  ✓[0m[90m handle negative integers[0m
    [32m  ✓[0m[90m handle positive and negative integers[0m

[0m  View Function[0m
  [32m  ✓[0m[90m should have 2 parameters[0m
  [32m  ✓[0m[90m should have an initPlayField method[0m


[92m [0m[32m 91 passing[0m[90m (257ms)[0m
[31m  5 failing[0m

[0m  1) InputInterface Function inputInterface.startListening Method should set key down event listeners and handle left arrow key input:
[0m[31m     ReferenceError: window is not defined[0m[90m
    at InputInterface.startListening (js/InputInterface.js:18:4)
    at Context.<anonymous> (tests/input_interface_test.js:46:19)
  
[0m
[0m  2) InputInterface Function inputInterface.startListening Method should set key down event listeners and handle up arrow key input:
[0m[31m     ReferenceError: window is not defined[0m[90m
    at InputInterface.startListening (js/InputInterface.js:18:4)
    at Context.<anonymous> (tests/input_interface_test.js:54:19)
  
[0m
[0m  3) InputInterface Function inputInterface.startListening Method should set key down event listeners and handle right arrow key input:
[0m[31m     ReferenceError: window is not defined[0m[90m
    at InputInterface.startListening (js/InputInterface.js:18:4)
    at Context.<anonymous> (tests/input_interface_test.js:62:19)
  
[0m
[0m  4) InputInterface Function inputInterface.startListening Method should set key down event listeners and handle down arrow key input:
[0m[31m     ReferenceError: window is not defined[0m[90m
    at InputInterface.startListening (js/InputInterface.js:18:4)
    at Context.<anonymous> (tests/input_interface_test.js:70:19)
  
[0m
[0m  5) InputInterface Function inputInterface.stopListening Method should stop key down event listeners:
[0m[31m     ReferenceError: window is not defined[0m[90m
    at InputInterface.startListening (js/InputInterface.js:18:4)
    at Context.<anonymous> (tests/input_interface_test.js:84:19)
  
[0m


