/**
	 * POINT OBJECT
	 *
	 * A point has a place in the grid and can be passed
	 * to View for drawing.
	 */

function Point(left,top){
	this.left = left;
	this.top = top;
	
	// Check if this point collides with another
	this.collidesWith = function(otherPoint){
		if(this.left === otherPoint.left && this.top === otherPoint.top){
			return true;
		}
		return false;
	}
}
