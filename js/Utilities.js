

var Utilities = function(){
	
	//returns sign of a number
	this.sign = function(number){
		if(number > 0){
			return 1;
		} else if(number < 0){
			return -1;
		} else if(number === 0){
			return 0;
		} else {
			return undefined;
		}
	};
	
	this.oppositeDirections = function(direction1,direction2){
		if((Math.abs(direction1) === Math.abs(direction2)) && (this.sign(direction1) * this.sign(direction2) === -1)){
			return true;
		} else {
			return false;
		}
	};
	
	this.mergeObjects = function(slave,master){
		
		if(Object.keys(slave).length === 0){
			return master;
		}
		var merged = {};
		
		for(var key in slave){
			if(typeof master[key] === "undefined"){
				merged[key] = slave[key];
			} else {
				merged[key] = master[key];
			}
		}
		
		return merged;
	}
	
	this.randomInteger = function(min,max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
}
