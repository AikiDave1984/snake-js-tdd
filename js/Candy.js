
if(typeof constants === 'undefined'){
	throw new Error('constants not defined');
}

if(typeof defaultConfig === 'undefined'){
	throw new Error('defaultConfig not defined');
}

/**
	 * CANDY OBJECT
	 * 
	 * @param point The point object which determines the position of the candy
	 * @param type Any type defined in constants.CANDY_*
	 */
var Candy =function(point,type){
	this.point = point;
	this.type = type;
	this.score = 0;//imcrement score when eaten by snake
	this.calories = 0;//how much growth the snake gains after eating this candy
	this.color = '#FFF';//colour of the candy
	this.radius = 0; //radius of the candy relative to config.pointSize
	this.decrement = 0;//if greater than 0, the candy will shrink
	this.minRadius = 0; //min value before disappearing
	
	switch(type){
		case constants.CANDY_REGULAR:
			this.score = 5;
			this.calories =3;
			this.radius = 0.3;
			this.color = defaultConfig.candyColor;
			break;
		case constants.CANDY_MASSIVE:
			this.score = 15;
			this.calories =5;
			this.radius = 0.45;
			this.color = defaultConfig.candyColor;
			break;
		case constants.CANDY_SHRINKING:
			this.score = 50;
			this.calories =0;
			this.radius = 0.45;
			this.decrement = 0.008;
			this.minRadius = 0.05;
			this.color = defaultConfig.shrinkingCandyColor;
			break;
	}
	
	// Shrinks a CANDY_SHRINKING candy. Returns false if candy is below minRadius
	this.age = function(){
		
		if(this.type=== constants.CANDY_SHRINKING ){
			this.radius -= this.decrement;
			if(this.radius > this.minRadius){
				return true;
			}else {
				return false;
			}
		} else {	
			return true;
		}

	}
}
