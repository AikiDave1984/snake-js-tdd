function InputInterface(pauseFn, resumeFn, autoPlayFn){
	
	var lastDirection = null; // Corresponds to the last arrow key pressed
	var listening = false; // Listening right now for key strokes etc?
	var arrowKeys = [37,38,39,40]; // Key codes for the arrow keys on a keyboard
	

	
	
	/**
		 * Public methods below
	*/
	
	this.lastDirection = function(){
		return lastDirection;
	}
	
	// Start listening for player events
	this.startListening = function(){
		if(!listening){
			window.addEventListener('keydown',handleKeyDown,true);
			window.addEventListener('blur',pauseFn,true);
			window.addEventListener('focus',resumeFn,true);
			listening = true;
		}
	}
	
	// Stop listening for events. Typically called at game end
	this.stopListening = function(){
		if(listening){
			window.removeEventListener('keydown',handleKeyDown,true);
			window.removeEventListener('blur',pauseFn,true);
			window.removeEventListener('focus',resumeFn,true);
			listening = false;
		}
	}
	
	/**
		 * Private methods below
		 */
	
	var handleKeyDown = function(event){
		// If the key pressed is an arrow key
		if(arrowKeys.indexOf(event.which) >=0){
			event.preventDefault();
			handleArrowKeyPress(event);
		}
	}
	
	var handleArrowKeyPress = function(event){
		with(constants){
			switch(event.which){
				case 37:
					lastDirection = DIRECTION_LEFT;
					break;
				case 38:
					lastDirection = DIRECTION_UP;
					break;
				case 39:
					lastDirection = DIRECTION_RIGHT;
					break;
				case 40:
					lastDirection = DIRECTION_DOWN;
					break;
			}
		}
		// Arrow keys usually makes the browser window scroll. Prevent this evil behavior
		event.preventDefault();
		// Call the auto play function
		autoPlayFn();
	}

	/* test-code */
	this.api = {};
	this.api._arrowKeys = arrowKeys;
	this.api._lastDirection = lastDirection;
	this.api._listening = listening;
	this.api._handleKeyDown = handleKeyDown;
	/* end-test-code */

}

