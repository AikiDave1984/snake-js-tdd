function Config(config){

	var utilities = new Utilities();

	return config ? utilities.mergeObjects(defaultConfig, config) : defaultConfig;
}