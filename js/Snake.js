
if(typeof constants === 'undefined'){
	throw new Error('constants not defined');
}


function Snake(){
	this.direction = constants.DEFAULT_DIRECTION;
	this.points = [];
	this.growthLeft = 0;
	this.isAlive = true;
	
	this.collidesWith = function(point,simulateMovement){
		
		if(this.growthLeft ===0 && simulateMovement){
			var range = this.points.length - 1;
		} else {
			var range = this.points.length;
		}
		
		
		for(var i =0; i<range;i++){
			if(point.collidesWith(this.points[i])){
				return true;
			}
		}
		return false;
	}
}
