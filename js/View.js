


function View(parentElement,backgroundColor){

	var playField,		  //DOM <canvas> element
			ctx,		 				//The canvas context
			snakeThickness; //Thickness of the snake in pixels

	this.initPlayField = function(){
		
		if(typeof config === 'undefined'){
			config = new Config();
		}
		snakeThickness = length(0.9);
		
		playField = document.createElement("canvas");
		playField.setAttribute('id','snake-js');
		playField.setAttribute('width',config.gridWidth * config.pointSize);
		playField.setAttribute('height',config.gridHeight * config.pointSize + constants.SCOREBOARD_HEIGHT);
		parentElement.appendChild(playField);
		ctx = playField.getContext("2d");
		ctx.translate(0,constants.SCOREBOARD_HEIGHT);
	}
	
	this.drawSnake = function(snake, color){
		/* test-code */
		if(typeof this.api._ContextTester !== 'undefined'){
			ctx = new this.api._ContextTester(playField);
			this.api._ctx = ctx;
		}
		/* end-test-code */
		
		// If there is only one point
		if (snake.points.length === 1) {
			var position = getPointPivotPosition(snake.points[0]);

			ctx.fillStyle = color;
			ctx.beginPath();
			ctx.arc(position.left, position.top, snakeThickness/2, 0, 2*Math.PI, false);
			ctx.fill();
		} else {
			
			// Prepare drawing
			ctx.strokeStyle = color;
			ctx.lineWidth = snakeThickness;
			ctx.lineJoin = "round";
			ctx.lineCap = "round";
				
			// Bein path drawing.
			ctx.beginPath();
			
			for(var i =0;i<snake.points.length; i++){
				
				//shortname for point
				var currentPoint = snake.points[i];
			
				
				//looking at the head
				if(i===0){
					//The position of this point in screen pixels
					var currentPointPosition = getPointPivotPosition(currentPoint);
					//Don't draw anything, move the "pencil" to the position o the head
					ctx.moveTo(currentPointPosition.left,currentPointPosition.top);
				}
				//looking at any other point
				else {
					//Short name to previous point (looked at in last iteration)
					var prevPoint = snake.points[i-1];
					
					
					
					//If these points are next to each other(Snake didn't go through wall)
					if(Math.abs(prevPoint.left-currentPoint.left) <=1 && Math.abs(prevPoint.top-currentPoint.top) <= 1){
						//position of this point in screen pixels
						var currentPointPosition = getPointPivotPosition(currentPoint);
						//Draw pencil from the position of the "pencil" to this point
						ctx.lineTo(currentPointPosition.left,currentPointPosition.top);
					}
				}
			}
			
			ctx.stroke();
		}
	
	}
	
	var length = function(value){
		
		if(typeof config === 'undefined'){
			var config = new Config();
		}
		return value * config.pointSize;
	}
	
	var getPointPivotPosition = function(point){
		var position = {
			left:point.left * length(1) +length(.5),
			top:point.top * length(1) + length(.5)
		};
		
		return position;
	}

	/* test-code */
	this.api = {};
	this.api._playField = playField;
	this.api._ctx = ctx;
	this.api._snakeThickness = snakeThickness;
	this.api._length = length;
	this.api._getPointPivotPosition = getPointPivotPosition;
	this.api._ContextTester = ContextTester;
	/* end-test-code */


}

